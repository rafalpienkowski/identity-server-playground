﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("http://localhost:5000");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }
            
            // request token
//            var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");
//            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api2");
//
//            if (tokenResponse.IsError)
//            {
//                Console.WriteLine(tokenResponse.Error);
//                return;
//            }
//            
//            Console.WriteLine(tokenResponse.Json);
            
            var tokenResponse2 = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "ro.client",
                ClientSecret = "secret",

                UserName = "alice",
                Password = "password",
                Scope = "api1"
            });

            Console.WriteLine(tokenResponse2.Json);

            
        }
    }
}