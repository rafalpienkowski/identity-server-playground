using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace IdSrv
{
    public class Config
    {
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "api_client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "api1" }
                },
                
                new Client
                {
                    ClientId = "api_client_2",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret_2".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "api2" }
                },
                
                // OpenID Connect implicit flow client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    
                    AllowOfflineAccess = true,

                    // where to redirect to after login
                    RedirectUris = { "http://localhost:5003/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "http://localhost:5003/signout-callback-oidc" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                },
                
                new Client
                {
                    ClientId = "mvc2",
                    ClientName = "Second MVC Client",
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    
                    ClientSecrets =
                    {
                        new Secret("secret2".Sha256())
                    },
                    
                    AllowOfflineAccess = true,

                    // where to redirect to after login
                    RedirectUris = { "http://localhost:5004/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "http://localhost:5004/signout-callback-oidc" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api2"
                    }
                },
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(), 
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource
                {
                    Name = "mvc",

                    Scopes =
                    {
                        new Scope()
                        {
                            Name = "mvc.full_access",
                            DisplayName = "Full access to MVC",
                            UserClaims = new List<string>{ "location"}
                        },
                        new Scope
                        {
                            Name = "mvc.read_only",
                            DisplayName = "Read only access to MVC",
                            UserClaims = new List<string>{ JwtClaimTypes.Email }
                        }
                    }
                },
                new ApiResource("api1", "Primary API", new []{"location", "awesome"}),
                new ApiResource("api2", "Secondary API", new []{"location", "hads"}),
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "b",

                    Claims = new []
                    {
                        new Claim("name", "Alice"),
                        new Claim("website", "https://alice.com"),
                        new Claim("awesome","Yeah, I'm awesome"),
                        new Claim("location","Alice location"), 
                        new Claim("hads","HADS admin"), 
                        
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "b",

                    Claims = new []
                    {
                        new Claim("name", "Bob"),
                        new Claim("website", "https://bob.com"),
                        new Claim("email", "admin@bob.com"),
                        new Claim("awesome","Uuupsss, I not awesome :("),
                        new Claim("location","Bob's location"),
                        new Claim("hads","HADS user"),
                    }
                }
            };
        }
    }
}